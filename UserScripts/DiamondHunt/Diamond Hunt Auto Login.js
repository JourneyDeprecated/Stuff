// ==UserScript==
// @name         Diamond Hunt Auto Login
// @namespace    Diamond Hunt Auto Login
// @version      0
// @description  Diamond Hunt Auto Login after 10 minutes.
// @author       JourneyOver
// @match        https://www.diamondhunt.co/
// @grant        none
// ==/UserScript==

autoLogin = function()
{
    document.getElementById("username").value = "Null";
    document.getElementById("password").value = "Null";
    document.getElementById("loginSubmitButton").click();
};

setInterval(autoLogin, 600000);

//then change the "Null" with your own username and password in the respective places.