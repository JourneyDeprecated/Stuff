// ==UserScript==
// @name		Diamond Hunt Pro
// @namespace	Diamond Hunt Pro
// @version		0.2
// @description	Diamond Hunt Pro
// @author		Journey Over
// @match		*.diamondhunt.co/game.php
// @updateURL https://raw.githubusercontent.com/JourneyOver/Stuff/gh-pages/UserScripts/DiamondHunt/Diamond%20Hunt%20Pro%20US%20Loader.js
// @require		https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
// @require		https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js
// @grant		unsafeWindow
// ==/UserScript==

'use strict';
/*
NOTES:
function openFarmingDialogue(id) - in dialogues
function plantSeed(seed) - in dialogues
*/

// Variables
var oresAndBars = [["sand", "glass"], ["copper", "bronze"],["iron", "iron"], ["silver", "silver"], ["gold", "gold"], ["promethium", "promethium"]]; // 2D Array containing ores and bar types
var exploringAreas = ["fields", "forest", "caves", "northFields", "volcano", "ocean", "desert", "swamp", "mansion", "pyramid", "dragonsLair"];

// User settings - Choose which features you want to run

// User config, EDIT THESE
var BAR_TYPE_TO_SMELT = oresAndBars[4]; // 0 = glass, 1 = bronze, 2 = iron, 3 = silver, 4 = gold, 5 = promethium
var AREA_TO_EXPLORE = exploringAreas[8]; // 0 = fields, 1 = forest, 2 = caves, 3 = northern fields, etc
var REQUIRE_BOUND_ROBOT = true; // If set to true, requires a robot to be bound to use it
var REQUIRE_BOUND_ROCKET = true; // If set to true, requires a rocket to be bound to use it
var MINIMUM_ROBOT_LEVEL = 80; // Set the minimum mining level you want for the robot to run
var MINIMUM_ROCKET_LEVEL = 80; // Set the minimum mining level you want for the rocket to run

// Flags - Do nothing atm
var furnaceFlag = 0;
var robotFlag = 0;
var rocketFlag = 0;
var sawmillFlag = 0;

// Run the main loop when the page loads
unsafeWindow.onload = function () {
	main();
}

// Our main loop, repeats itself between every 25 to 100 seconds
function main() {
	enableFarmingTimer();
	enableWizard();
	enablePlanter();

	if (unsafeWindow.username == 'journeyover') {
		doFurnace(BAR_TYPE_TO_SMELT);
		doRobot();
		doSeedPotion();
		doStardustPotions();
		doExploring(AREA_TO_EXPLORE);
	}
	setTimeout(main, getRandomTimeMillis(25, 100));
}

// Enable the donator farming timer (hurray for client-side code!)
function enableFarmingTimer() {
	if (unsafeWindow.farmingTimer != 1) {
		unsafeWindow.farmingTimer = 1;
	}
}

// Enable the Wizard without owning it (hurray for client-side code!)
function enableWizard() {
	if (unsafeWindow.wizard != 1) {
		unsafeWindow.wizard = 1;
	}
}

// Enable the planter without owning it (hurray for client-side code!)
function enablePlanter() {
	if (unsafeWindow.bindedPlanter != 1) {
		unsafeWindow.bindedPlanter = 1;
	}
}

// Run the furnace if it isn't already running and you have enough ores
// There is no check for tin, we're assuming your copper matches your tin since that's usually the case
function doFurnace(barType) {
	//console.log("Running furnace");
	/* barType[0] contains the ore name, barType[1] contains the bar name */
	if (unsafeWindow.furnacePerc == 0 && unsafeWindow.furnaceTotalTimer == 0) {
		unsafeWindow.amountToSmeltGlobal = unsafeWindow.getFurnaceCapacityAgain(unsafeWindow.bindedFurnaceLevel);
		if (unsafeWindow[barType[0]] >= unsafeWindow.amountToSmeltGlobal) { /* Call a variable in unsafeWindow based on a variable name in this script */
			unsafeWindow.oresAndBarselectedToSmeltGlobal = barType[1];
			send("SMELT=" + unsafeWindow.oresAndBarselectedToSmeltGlobal + ";" + unsafeWindow.amountToSmeltGlobal);
		}
	}
}

// Send out a rocket if your mining level is at or above the configured level and you have rocket fuel
// Craft rocket fuel if you have 150M+ oil (to not lose refinery perk)
// Craft and use mining potions when the rocket is within 45 minutes of landing ***NOT YET ADDED**
function doRocket() {
	if ((!REQUIRE_BOUND_ROCKET || unsafeWindow.bindedRocket) && unsafeWindow.getLevel(unsafeWindow.miningXp) >- MINIMUM_ROCKET_LEVEL) {
		if (unsafeWindow.rocketTimer == 0) {
			if (unsafeWindow.rocketFuel > 0) {
				unsafeWindow.send("sendRocketToMoon");
				setTimeout(closeDialogue, 5000);
			} else if (Math.floor((unsafeWindow.oil-100000000)/50000000) >= 1 && unsafeWindow.canCraftItem("rocketFuel")) {
				unsafeWindow.send("CRAFT=rocketFuel");
			}
		} else if (unsafeWindow.rocketTimer <= 999999999999/*fix this later - drink a mining potion, also add check for mining potion before sending rocket and maybe mining potion brewing*/) {

		}
	}
}

// Send out a robot if your mining level is at or above the configured level and you have rocket fuel
// Craft rocket fuel if you have 150M+ oil (to not lose refinery perk)
function doRobot() {
	if ((!REQUIRE_BOUND_ROBOT || unsafeWindow.bindedRobot) && unsafeWindow.getLevel(unsafeWindow.miningXp) >= MINIMUM_ROBOT_LEVEL && unsafeWindow.robotTimer == 0) {
		if (unsafeWindow.rocketFuel > 0) {
			unsafeWindow.send("sendRobotUnderground");
			setTimeout(closeDialog, 5000);
		} else if (Math.floor((unsafeWindow.oil-100000000)/50000000) >= 1 && unsafeWindow.canCraftItem("rocketFuel")) {
			unsafeWindow.send("CRAFT=rocketFuel");
		}
	}
}

// Explore the given area if we have enough energy (factors energy reduction)
// Opens loot bags if we have 5 or more for the given area
function doExploring(area) {
	//console.log("Sending Explorer");
	if (unsafeWindow.exploringTimer == 0 && unsafeWindow.energy >= unsafeWindow.getAreaEnergy(area) * ((100 - unsafeWindow.exploringEnergyReductionPerc)/100)) {
		var t = 0;
		if (unsafeWindow[area + "Loot"] >= 4) {
			unsafeWindow.send("OPEN_LOOT=" + area + "Loot");
			setTimeout(closeDialog, 1000);
			t = getRandomTimeMillis(3, 7); // Set a timeout if we opened the bag first
		}
		setTimeout(function() { unsafeWindow.send("EXPLORE=" + area); }, t);
	}
}

// Drink normal stardust potions whenever your potion timer runs out, so long as you have them and your stardust is below 2 billion
function doStardustPotion() {
	if (unsafeWindow.starDustPotion > 0 && unsafeWindow.starDustPotionTimer == 0 && unsafeWindow.starDust < 2000000000) {
		unsafeWindow.send("DRINK=starDustPotion");
		// Drinks another potion shortly after if you have the potion stacker donor buff for some reason
		if (unsafeWindow.potionStacker != 0) {
			setTimeout(function() { unsafeWindow.send("DRINK=starDustPotion"); }, getRandomTimeMillis(1, 3));
		}
	}
}

// Drink Seed potions whenever your potion timer runs out, so long as you have them
function doSeedPotion() {
	if (unsafeWindow.seedPotion > 0 && unsafeWindow.seedPotionTimer == 0) {
		unsafeWindow.send("DRINK=seedPotion");
		// Drinks another potion shortly after if you have the potion stacker donor buff for some reason
		if (unsafeWindow.potionStacker != 0) {
			setTimeout(function() { unsafeWindow.send("DRINK=seedPotion"); }, getRandomTimeMillis(1, 3));
		}
	}
}

// Check vendor for certain items at or below a certain price and buy them
// Convert plat to coins if you have to
function checkVendor() {

}

// Check the market for certain items at or below a certain price
// Probably only notify you of their existence rather than buy them
function checkMarket() {

}

// Drinks any stardust potions that are available, but only one at a time (I read it's buggy when you stack them), starting with the highest type
// Only drinks if you have under 2 billion stardust
function doStardustPotions() {
	if (unsafeWindow.starDust < 2000000000 && unsafeWindow.starDustPotionTimer == 0 && unsafeWindow.superStarDustPotionTimer == 0 && unsafeWindow.megaStarDustPotionTimer == 0) {
		var amt = 0;
		var send;
		if (amt = unsafeWindow.megaStarDustPotion > 0) {
			send = "DRINK=megaStarDustPotion";
		} else if (amt = unsafeWindow.superStarDustPotion > 0) {
			send = "DRINK=superStarDustPotion";
		} else if (amt = unsafeWindow.starDustPotion) {
			send = "DRINK=starDustPotion";
		}
		if (amt > 0 && send != undefined) {
			unsafeWindow.send(send);
			// Drinks another potion shortly after if you have the potion stacker donor buff for some reason, and more than one of the potion type
			if (amt > 1 && unsafeWindow.potionStacker != 0) {
				setTimeout(function() { unsafeWindow.send(send); }, getRandomTimeMillis(1, 3));
			}
		}
	}
}

// Close jQuery dialogue box
// Not quite working yet I don't think
function closeDialog() {
	if ($("#dialog").hasClass('ui-dialog-content')) {
		$("#dialog").dialog("destroy");
	}
}

// Get a random time in milliseconds between two inclusive seconds
function getRandomTimeMillis(min, max) {
	min = min * 1000;
	max = max * 1000;
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
