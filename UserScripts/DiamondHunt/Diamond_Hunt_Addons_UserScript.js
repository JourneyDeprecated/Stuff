// ==UserScript==
// @name         Diamond Hunt Addons
// @namespace    Diamond Hunt Addons
// @version      0.2
// @description  Diamond Hunt Oil Timer + Ores per hour
// @author       JourneyOver
// @match        http://www.diamondhunt.co/game.php*
// @updateURL   https://raw.githubusercontent.com/JourneyOver/Stuff/gh-pages/UserScripts/DiamondHunt/Diamond%20Hunt%20Addons%20US%20Loader.js
// @grant        none
// ==/UserScript==

//ore gains
{
var ores = ["wood", "sand", "stone", "copper", "tin", "iron", "silver", "gold", "quartz", "flint", "marble", "titanium", "promethium", "runite", "dragonstone", "glass", "bronzeBar", "ironBar", "silverBar", "goldBar", "promethiumBar", "runiteBar"];
var oldOreValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

for (i = 0; i < ores.length; i++) {
  if ($('#item-' + ores[i] + '-box > #' + ores[i] + '-gains').length) {

  } else {
    text = '<span id="' + ores[i] + '-gains"></span>';
    if (ores[i] != "sand" && ores[i] != "stone" && ores[i] != "wood") {
      text = '<br />' + text;
    }
    $('#item-' + ores[i] + '-box').append(text);
    oldOreValues[i] = window[ores[i]];
  }
}

$('.inventory-item-box').each(function(i, val) {
  $(val).height(150);
});

updateOreGains = function() {
  for (i = 0; i < ores.length; i++) {
    $('#' + ores[i] + '-gains').text((window[ores[i]] - oldOreValues[i]) * 12 + "/h");
    oldOreValues[i] = window[ores[i]];
  }
};

setInterval(updateOreGains, 300000);
}

//oil timer
updateOilTimer = function() {

  if ($('#oilTimeLeft-statusbar').length) {
    oilUsage = oilLosePerSeconds - oilPerSeconds;
    if (oilUsage <= 0) {
      $('#oilTimeLeft-statusbar').text('Infinite oil');
    } else {
      seconds = Math.floor(oil / oilUsage);
      var numdays = Math.floor(seconds / 86400);
      var numhours = Math.floor((seconds % 86400) / 3600);
      var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
      var numseconds = ((seconds % 86400) % 3600) % 60;

      $('#oilTimeLeft-statusbar').text(numdays + " days " + numhours + " hours " + numminutes + " minutes " + numseconds + " seconds");
    }
  } else {
    oilColumn = 2;
    span = '<br /><span id="oilTimeLeft-statusbar"></span></td>';
    $('.top-status-bar td:nth-child(' + oilColumn + ')').append(span);
    $('.top-status-bar td:nth-child(' + oilColumn + ') > img').css('float', 'left');
  }
};
setInterval(updateOilTimer, 3000);